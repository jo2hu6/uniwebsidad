using System;

namespace Uniwebsidad.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public string Descripcion { get; set; }
        public int IdCurso{ get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}