﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowUniwebsidad.Pages
{
    class CrearCursoPage
    {
        public IWebDriver WebDriver { get; }

        public CrearCursoPage(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }


        //UI elements
        public IWebElement linkLogin => WebDriver.FindElement(By.LinkText("Iniciar Sesión"));

        public IWebElement txtUsername => WebDriver.FindElement(By.Name("Username"));

        public IWebElement txtPassword => WebDriver.FindElement(By.Name("Password"));

        public IWebElement btnLogin => WebDriver.FindElement(By.CssSelector(".btnLogin"));

        public IWebElement linkMiLista => WebDriver.FindElement(By.LinkText("Mi Lista"));


        public void ClickLogin() => linkLogin.Click();

        public void Login(string userName, string password)
        {
            txtUsername.SendKeys(password);
            txtPassword.SendKeys(userName);
        }

        public void ClickLoginButton() => btnLogin.Submit();




        public IWebElement linkMisCursos => WebDriver.FindElement(By.LinkText("Mis Cursos"));

        public IWebElement txtNombre => WebDriver.FindElement(By.Name("Curso"));

        public IWebElement txtCategoria => WebDriver.FindElement(By.Name("Categoria"));

        public IWebElement btnCrearCurso => WebDriver.FindElement(By.CssSelector(".btnCrearCurso"));

        public IWebElement btnCrear => WebDriver.FindElement(By.CssSelector(".btnCrear"));


        public void ClickCrear() => linkMisCursos.Click();


        public void ClickCrearCurso() => btnCrear.Click();

        public void Crear(string curso, string categoria)
        {
            txtNombre.SendKeys(curso);
            txtCategoria.SendKeys(categoria);
        }

        public void ClickCrearCursoI() => btnCrearCurso.Submit();

        public bool IsMiListaExist() => linkMiLista.Displayed;
    }
}
