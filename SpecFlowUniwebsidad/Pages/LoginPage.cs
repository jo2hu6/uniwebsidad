﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlow.Uniwebsidad.Test.Pages
{
    public class LoginPage
    {
        public IWebDriver WebDriver { get; }

        public LoginPage(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }


        //UI elements
        public IWebElement linkLogin => WebDriver.FindElement(By.LinkText("Iniciar Sesión"));

        public IWebElement txtUsername => WebDriver.FindElement(By.Name("Username"));

        public IWebElement txtPassword => WebDriver.FindElement(By.Name("Password"));

        public IWebElement btnLogin => WebDriver.FindElement(By.CssSelector(".btnLogin"));

        public IWebElement linkMiLista => WebDriver.FindElement(By.LinkText("Mi Lista"));


        public void ClickLogin() => linkLogin.Click();

        public void Login(string userName, string password)
        {
            txtUsername.SendKeys(password);
            txtPassword.SendKeys(userName);
        }

        public void ClickLoginButton() => btnLogin.Submit();


        public bool IsMiListaExist() => linkMiLista.Displayed;

    }
}