﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlow.Uniwebsidad.Test.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Assert = NUnit.Framework.Assert;

namespace SpecFlow.Uniwebsidad.Test.Steps
{
    [Binding]
    public sealed class LoginSteps
    {
        //Anti-Context Injection Code  -  100% wrong
        LoginPage loginPage = null;



        // Step definitions  
        [Given(@"I launch the aplication")]
        public void GivenILaunchTheAplication()
        {

            IWebDriver webDriver = new ChromeDriver();

            webDriver.Navigate().GoToUrl("https://localhost:44341");
            loginPage = new LoginPage(webDriver);


        }


        [Given(@"I click login link")]
        public void GivenIClickLoginLink()
        {

            loginPage.ClickLogin();

        }

        [Given(@"I enter the following details")]
        public void GivenIEnterTheFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();

            loginPage.Login((string)data.Username, (string)data.Password);

        }

        [Given(@"I click Iniciar sesion")]
        public void GivenIClickIniciarSecion()
        {
            loginPage.ClickLoginButton();
        }

        [Then(@"I shold see Mis cursos link")]
        public void ThenISholdSeeMisCorsosLink()
        {
            Assert.That(loginPage.IsMiListaExist(), Is.True);
        }


    }
}