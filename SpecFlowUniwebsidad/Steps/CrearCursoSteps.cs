﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlowUniwebsidad.Pages;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SpecFlowUniwebsidad.Steps
{
    [Binding]
    public class CrearCursoSteps
    {
        CrearCursoPage loginPage = null;

        IWebDriver webDriver = null;

        // Step definitions  
        [Given(@"I launch the aplication")]
        public void GivenILaunchTheAplication()
        {

            webDriver = new ChromeDriver();

            webDriver.Navigate().GoToUrl("https://localhost:44341/");
            loginPage = new CrearCursoPage(webDriver);


        }


        [Given(@"I click login link")]
        public void GivenIClickLoginLink()
        {

            loginPage.ClickLogin();

        }

        [Given(@"I enter the following details")]
        public void GivenIEnterTheFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();

            loginPage.Login((string)data.Username, (string)data.Password);

        }

        [Given(@"I click Iniciar sesion")]
        public void GivenIClickIniciarSecion()
        {
            loginPage.ClickLoginButton();
        }


        [Given(@"click Mis cursos")]
        public void GivenClickMisCursos()
        {
            loginPage.ClickCrear();
        }

        [Given(@"click Crear curso")]
        public void GivenClickCrearCurso()
        {
            loginPage.ClickCrearCurso();
        }

        [Given(@"Ingresar datos")]
        public void GivenIngresarDatos(Table table)
        {
            dynamic data = table.CreateDynamicInstance();

            loginPage.Crear((string)data.Curso, (string)data.Categoria);
        }

        [Given(@"Click en Crear")]
        public void GivenClickEnCrear()
        {
            loginPage.ClickCrearCursoI();
        }




        [Then(@"I shold see Mis cursos link")]
        public void ThenISholdSeeMisCorsosLink()
        {
            Assert.That(loginPage.IsMiListaExist(), Is.True);
        }
    }
}
